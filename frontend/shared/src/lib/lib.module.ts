import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [CommonModule],
  imports: [],
  exports: [],
})
export class LibModule {}
