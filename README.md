# School

# TODO list

- Initialize Angular frontend
  - Actual school app
  - School Admin app
  - Product Admin App
  - SandBox App

# Terminal Command Used

ng new school --skipInstall=true --minimal=true --createApplication=false --skipGit=true --style=scss --skipTests=true --prefix=sz --packageManager=yarn --newProjectRoot=frontend --directory=./

ng g application school --minimal=true --routing --style=scss --prefix=sz --skipTests=true --inlineTemplate=false --inlineStyle=false

ng g application admin --minimal=true --routing --style=scss --prefix=sz --skipTests=true --inlineTemplate=false --inlineStyle=false

ng g application super --minimal=true --routing --style=scss --prefix=sz --skipTests=true --inlineTemplate=false --inlineStyle=false

ng g application sandbox --minimal=true --routing --style=scss --prefix=sz --skipTests=true --inlineTemplate=false --inlineStyle=false
