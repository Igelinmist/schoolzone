import dotenv = require('dotenv');

const result = dotenv.config();

if (result?.error) {
  throw new Error('Add .env file');
}

export const config = {
  env: process.env.SZ_ENV,
  appName: process.env.SZ_APP,
  version: process.env.SZ_VERSION,
  port: process.env.SZ_PORT,
  mongoUri: `mongodb+srv://${process.env.SZ_MONGO_USER}:${process.env.SZ_MONGO_PASS}@${process.env.SZ_MONGO_HOST}.${process.env.SZ_MONGO_REPLICA}/${process.env.SZ_MONGO_DB}?retryWrites=true&w=majority`,
};
