import { config } from './config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const { appName, port, version } = config;
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix(version);
  await app.listen(port, () => {
    console.log(`${appName} is running on port ${port}/${version}`);
  });
}
bootstrap();
